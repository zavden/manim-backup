from manim import *
from random import uniform
from itertools import zip_longest

def grouper(iterable, n, *, incomplete='ignore', fillvalue=np.uint8([0])):
    """
    Collect data into non-overlapping fixed-length chunks or blocks

        >>> grouper('ABCDEFG', 3, fillvalue='x')
        [ABC] [DEF] [Gxx]

        >>> grouper('ABCDEFG', 3, incomplete='strict')
        [ABC] [DEF] ValueError

        >>> grouper('ABCDEFG', 3, incomplete='ignore')
        [ABC] [DEF]
    """
    args = [iter(iterable)] * n
    if incomplete == 'fill':
        return zip_longest(*args, fillvalue=fillvalue)
    if incomplete == 'strict':
        return zip(*args, strict=True)
    if incomplete == 'ignore':
        return zip(*args)
    else:
        raise ValueError('Expected fill, strict, or ignore')

class Montecarlo(Scene):
    def construct(self):
        SQUARE_COLOR = BLUE
        CIRCLE_COLOR = RED
        SIDE_LENGTH  = 6
        COUNTER = 0
        TOTAL_DOTS = int(3000)
        RUN_TIME = 10

        sq = Square(side_length=SIDE_LENGTH, color=SQUARE_COLOR)
        c  = Circle(radius=SIDE_LENGTH/2, color=CIRCLE_COLOR)

        SQUARE_CENTER = sq.get_center()
        RADIUS        = SIDE_LENGTH / 2
        MIN_X, MAX_X = sq.get_left()[0], sq.get_right()[0]
        MIN_Y, MAX_Y = sq.get_bottom()[1], sq.get_top()[1]

        dots = VGroup(*[
            Dot( [uniform(MIN_X, MAX_X), uniform(MIN_Y, MAX_Y), 0],
                 radius=0.02, color=SQUARE_COLOR                 )
                    .set(_in_circle=False)
            for _ in range(TOTAL_DOTS)
        ])

        def is_inside_circle(d: Dot):
            if np.linalg.norm(d.get_center() - SQUARE_CENTER) <= RADIUS:
                d.set_color(CIRCLE_COLOR)
                d._in_circle = True
                nonlocal COUNTER; COUNTER += 1
                return True
            return False

        for _ in filter(is_inside_circle, dots): pass
        print(f"pi = {4 * COUNTER / TOTAL_DOTS}")

        self.add(sq, c)

        fps          = 15
        total_frames = fps * RUN_TIME
        count        = 0
        n_dots_in_circle = ValueTracker(0)
        n_dots_in_screen = ValueTracker(0)
        dots.set_opacity(0)
        tmp_mob = Square().set_opacity(0)
        # Group variables
        n_data_per_frame  = int(np.ceil(TOTAL_DOTS / total_frames))
        dots_packed       = list(grouper(dots, n_data_per_frame))
        n_dots_packed     = len(dots_packed)
        # Add leftover
        total_dots_packed = n_dots_packed * n_data_per_frame
        left_over = TOTAL_DOTS % total_dots_packed
        if left_over != 0: dots_packed.append(dots[-left_over:])

        def update_dots(mob, alpha):
            nonlocal count
            try:
                grp = VGroup(*dots_packed[count]).set_opacity(1)
                d_in_circle = [_d for _d in grp if _d._in_circle == True]
                n_dots_in_circle.increment_value(len(d_in_circle))
                n_dots_in_screen.increment_value(len(grp))
                count +=1
            except:
                pass

        counter_dots = Integer(0).next_to(sq, RIGHT, buff=1)\
                .add_updater(lambda mob: mob.set_value(n_dots_in_screen.get_value()))

        pi = DecimalNumber(0, num_decimal_places=5)\
                .next_to(counter_dots, UP, aligned_edge=LEFT)\
                .add_updater(lambda mob: mob.set_value(
                    4 * n_dots_in_circle.get_value() / n_dots_in_screen.get_value()
                ))

        self.add(dots, n_dots_in_circle, n_dots_in_screen, counter_dots, pi)
        self.play(
            UpdateFromAlphaFunc(
                tmp_mob, update_dots,
                run_time=RUN_TIME, rate_func=linear
            )
        )
        self.wait()
